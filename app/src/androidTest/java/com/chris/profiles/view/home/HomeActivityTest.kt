package com.chris.profiles.view.home

import android.content.Intent
import androidx.test.espresso.intent.Intents
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.chris.profiles.datasources.network.comments.CommentsServiceApi
import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet
import com.chris.profiles.datasources.network.posts.PostsServiceApi
import com.chris.profiles.datasources.network.posts.netsources.models.PostNet
import com.chris.profiles.datasources.network.users.UsersServiceApi
import com.chris.profiles.datasources.network.users.netsources.models.UserNet
import com.chris.profiles.espressoDaggerMockRule
import com.chris.profiles.extensions.fromJsonCollection
import com.chris.profiles.view.home.robots.postItem
import com.chris.profiles.view.postprofile.PostProfileActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import retrofit2.mock.Calls

@RunWith(AndroidJUnit4::class)
class HomeActivityTest {
    @get:Rule
    var rule = espressoDaggerMockRule()
    @get:Rule
    val activityRule = ActivityTestRule(HomeActivity::class.java, false, false)
    @Mock
    private lateinit var mockPostsServiceApi: PostsServiceApi
    @Mock
    private lateinit var mockUsersServiceApi: UsersServiceApi
    @Mock
    private lateinit var mockCommentsServiceApi: CommentsServiceApi

    @Before
    fun setup() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val posts = instrumentation.fromJsonCollection<List<PostNet>>("posts")
        `when`(mockPostsServiceApi.getPosts()).thenReturn(Calls.response(posts))

        val (postId, userId) = arrayOf(2, 1)
        val posts2 = instrumentation.fromJsonCollection<List<PostNet>>("post")
        `when`(mockPostsServiceApi.getPost(postId)).thenReturn(Calls.response(posts2))
        val users = instrumentation.fromJsonCollection<List<UserNet>>("user")
        `when`(mockUsersServiceApi.getUser(userId)).thenReturn(Calls.response(users))
        val comments = instrumentation.fromJsonCollection<List<CommentNet>>("comments")
        val email = users[0].email
        `when`(mockCommentsServiceApi.getComments(email!!)).thenReturn(Calls.response(comments))
    }

    @Test
    fun onLaunchDisplayPosts() {
        val (title1, title2) = arrayOf(
            "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            "qui est esse"
        )

        activityRule.launchActivity(Intent())

        postItem {
            matchTitle(0, title1)
            matchTitle(1, title2)
        }
    }

    @Test
    fun onItemClickLaunchPostProfileActivity() {
        activityRule.launchActivity(Intent())
        Intents.init()

        postItem {
            openOnClick(1, PostProfileActivity::class.java.name)
        }
    }
}