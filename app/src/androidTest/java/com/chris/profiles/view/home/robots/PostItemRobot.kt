package com.chris.profiles.view.home.robots

import com.chris.profiles.R
import com.chris.profiles.robots.BaseTestRobot

fun postItem(func: PostItemRobot.() -> Unit) = PostItemRobot()
    .apply { func() }

class PostItemRobot : BaseTestRobot() {
    fun matchTitle(position: Int, title: String) = matchListItem(R.id.posts_recycler_view, position, title)

    fun openOnClick(position: Int, activityName: String) {
        clickListItem(R.id.posts_recycler_view, position)
        launchActivity(activityName)
    }
}