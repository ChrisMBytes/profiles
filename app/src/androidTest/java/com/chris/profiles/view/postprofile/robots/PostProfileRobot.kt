package com.chris.profiles.view.postprofile.robots

import com.chris.profiles.R
import com.chris.profiles.robots.BaseTestRobot

fun postProfile(func: PostProfileRobot.() -> Unit) = PostProfileRobot()
    .apply { func() }


class PostProfileRobot : BaseTestRobot() {
    fun matchPostText(title: String, body: String) {
        matchText(textView(R.id.text_title), title)
        matchText(textView(R.id.text_body), body)
    }

    fun matchUserName(name: String) = matchText(textView(R.id.text_name), name)

    fun matchCommentTotal(total: String) = matchText(textView(R.id.text_total), total)
}