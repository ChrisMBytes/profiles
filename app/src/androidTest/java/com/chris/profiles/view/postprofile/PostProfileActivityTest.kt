package com.chris.profiles.view.postprofile

import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.chris.profiles.datasources.network.comments.CommentsServiceApi
import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet
import com.chris.profiles.datasources.network.posts.PostsServiceApi
import com.chris.profiles.datasources.network.posts.netsources.models.PostNet
import com.chris.profiles.datasources.network.users.UsersServiceApi
import com.chris.profiles.datasources.network.users.netsources.models.UserNet
import com.chris.profiles.espressoDaggerMockRule
import com.chris.profiles.extensions.fromJsonCollection
import com.chris.profiles.view.postprofile.robots.postProfile
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import retrofit2.mock.Calls

@RunWith(AndroidJUnit4::class)
class PostProfileActivityTest {
    @get:Rule
    var rule = espressoDaggerMockRule()
    @get:Rule
    val activityRule = ActivityTestRule(PostProfileActivity::class.java, false, false)
    @Mock
    private lateinit var mockPostsServiceApi: PostsServiceApi
    @Mock
    private lateinit var mockUsersServiceApi: UsersServiceApi
    @Mock
    private lateinit var mockCommentsServiceApi: CommentsServiceApi


    private fun launch(postId: Int, userId: Int) {
        val intent = Intent()
        intent.putExtra(PostProfileActivity.EXTRA_USER_ID, userId)
        intent.putExtra(PostProfileActivity.EXTRA_POST_ID, postId)
        activityRule.launchActivity(intent)
    }

    @Test
    fun onLaunchDisplayPostProfileData() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val (postId, userId) = arrayOf(1, 1)
        val posts = instrumentation.fromJsonCollection<List<PostNet>>("post")
        `when`(mockPostsServiceApi.getPost(postId)).thenReturn(Calls.response(posts))
        val users = instrumentation.fromJsonCollection<List<UserNet>>("user")
        `when`(mockUsersServiceApi.getUser(userId)).thenReturn(Calls.response(users))
        val comments = instrumentation.fromJsonCollection<List<CommentNet>>("comments")
        val email = users[0].email
        `when`(mockCommentsServiceApi.getComments(email!!)).thenReturn(Calls.response(comments))
        val (_, _, title, body) = posts[0]
        val name = users[0].name
        val total = comments.size.toString()

        launch(postId, userId)

        postProfile {
            matchPostText(title!!, body!!)
            matchUserName(name!!)
            matchCommentTotal(total)
        }
    }
}