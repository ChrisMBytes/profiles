package com.chris.profiles.extensions

import android.app.Instrumentation
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.BufferedReader
import java.io.InputStreamReader

inline fun <reified T : Any> Instrumentation.fromJson(path: String): T {
    val bufferedReader = BufferedReader(InputStreamReader(context.assets.open(path), "UTF-8"))
    return Gson().fromJson(bufferedReader, T::class.java)
}

inline fun <reified T : Any> Instrumentation.fromJsonCollection(path: String): T {
    val bufferedReader = BufferedReader(InputStreamReader(context.assets.open(path), "UTF-8"))
    val collectionType = object : TypeToken<T>() {}.type
    return Gson().fromJson(bufferedReader, collectionType)
}