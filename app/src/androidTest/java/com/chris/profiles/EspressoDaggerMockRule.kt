package com.chris.profiles

import androidx.test.platform.app.InstrumentationRegistry
import com.chris.profiles.di.AppComponent
import com.chris.profiles.di.DataSourcesModule
import com.chris.profiles.view.ProfilesApplication
import it.cosenonjaviste.daggermock.DaggerMock

fun espressoDaggerMockRule() = DaggerMock.rule<AppComponent>(DataSourcesModule()) {
    set { component -> component.inject(app) }
    customizeBuilder<AppComponent.Builder> { it.application(app) }
}

val app: ProfilesApplication get() = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as ProfilesApplication