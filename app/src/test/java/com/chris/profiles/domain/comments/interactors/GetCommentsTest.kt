package com.chris.profiles.domain.comments.interactors

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.domain.comments.CommentsRepositoryApi
import com.chris.profiles.domain.comments.models.Comment
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class GetCommentsTest {
    @Mock
    private lateinit var mockCommentsRepositoryApi: CommentsRepositoryApi<ForDeferredK>
    @Mock
    private lateinit var mockComment: Comment

    private lateinit var sut: GetCommentsApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = GetComments(mockCommentsRepositoryApi)
    }

    @Test
    fun `getComments should return Comment when call is successful`() {
        val email = "email"
        val comments = arrayListOf(mockComment)
        `when`(mockCommentsRepositoryApi.getComments(email)).thenReturn(DeferredK.just(comments))

        val result = sut.getComments(email)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(comments, it) })
        verify(mockCommentsRepositoryApi).getComments(email)
    }

    @Test
    fun `getComments should emmit error when call is unsuccessful`() {
        val email = "email"
        val error = Exception("error")
        `when`(mockCommentsRepositoryApi.getComments(email)).thenReturn(DeferredK.raiseError(error))

        val result = sut.getComments(email)
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockCommentsRepositoryApi).getComments(email)
    }
}