package com.chris.profiles.domain.posts.interactors

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.posts.models.Post
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class GetPostTest {
    @Mock
    private lateinit var mockPostsRepositoryApi: PostsRepositoryApi<ForDeferredK>
    @Mock
    private lateinit var mockPost: Post

    private lateinit var sut: GetPostApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = GetPost(mockPostsRepositoryApi)
    }

    @Test
    fun `getPost should return Post when call is successful`() {
        val postId = 1
        `when`(mockPostsRepositoryApi.getPost(postId)).thenReturn(DeferredK.just(mockPost))

        val result = sut.getPost(postId)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockPost, it) })
        verify(mockPostsRepositoryApi).getPost(postId)
    }

    @Test
    fun `getPost should emmit error when call is unsuccessful`() {
        val postId = 1
        val error = Exception("error")
        `when`(mockPostsRepositoryApi.getPost(postId)).thenReturn(DeferredK.raiseError(error))

        val result = sut.getPost(postId)
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockPostsRepositoryApi).getPost(postId)
    }
}