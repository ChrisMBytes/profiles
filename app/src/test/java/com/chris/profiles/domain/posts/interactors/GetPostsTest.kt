package com.chris.profiles.domain.posts.interactors

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.posts.models.Post
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class GetPostsTest {
    @Mock
    private lateinit var mockPostsRepositoryApi: PostsRepositoryApi<ForDeferredK>
    @Mock
    private lateinit var mockPost: Post

    private lateinit var sut: GetPostsApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = GetPosts(mockPostsRepositoryApi)
    }

    @Test
    fun `getPosts should return Posts when call is successful`() {
        val posts = arrayListOf(mockPost)
        `when`(mockPostsRepositoryApi.getPosts()).thenReturn(DeferredK.just(posts))

        val result = sut.getPosts()
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(posts, it) })
        verify(mockPostsRepositoryApi).getPosts()
    }

    @Test
    fun `getPosts should emmit error when call is unsuccessful`() {
        val error = Exception("error")
        `when`(mockPostsRepositoryApi.getPosts()).thenReturn(DeferredK.raiseError(error))

        val result = sut.getPosts()
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockPostsRepositoryApi).getPosts()
    }
}