package com.chris.profiles.domain.users.interactors

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.domain.users.UsersRepositoryApi
import com.chris.profiles.domain.users.models.User
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class GetUserTest {
    @Mock
    private lateinit var mockUsersRepositoryApi: UsersRepositoryApi<ForDeferredK>
    @Mock
    private lateinit var mockUser: User

    private lateinit var sut: GetUserApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = GetUser(mockUsersRepositoryApi)
    }

    @Test
    fun `getUser should return User when call is successful`() {
        val userId = 1
        `when`(mockUsersRepositoryApi.getUser(userId)).thenReturn(DeferredK.just(mockUser))

        val result = sut.getUser(userId)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockUser, it) })
        verify(mockUsersRepositoryApi).getUser(userId)
    }

    @Test
    fun `getUser should emmit error when call is unsuccessful`() {
        val userId = 1
        val error = Exception("error")
        `when`(mockUsersRepositoryApi.getUser(userId)).thenReturn(DeferredK.raiseError(error))

        val result = sut.getUser(userId)
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockUsersRepositoryApi).getUser(userId)
    }
}