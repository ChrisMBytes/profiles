package com.chris.profiles.data.comments.repositories

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.deferredk.monadDefer.monadDefer
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.data.comments.repositories.mappers.CommentMapper
import com.chris.profiles.datasources.network.comments.netsources.CommentsNetServiceApi
import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet
import com.chris.profiles.domain.comments.CommentsRepositoryApi
import com.chris.profiles.domain.comments.models.Comment
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CommentsRepositoryTest {
    private val monadDefer = DeferredK.monadDefer()
    @Mock
    private lateinit var mockCommentsNetServiceApi: CommentsNetServiceApi<ForDeferredK>
    @Mock
    private lateinit var mockCommentNet: CommentNet
    @Mock
    private lateinit var mockComment: Comment
    @Mock
    private lateinit var mockCommentMapper: CommentMapper

    private lateinit var sut: CommentsRepositoryApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = CommentsRepository(mockCommentsNetServiceApi, monadDefer, mockCommentMapper)
    }

    @Test
    fun `getComments should return Comments when successful`() {
        val email = "email"
        val comments = arrayListOf(mockCommentNet, mockCommentNet)
        `when`(mockCommentsNetServiceApi.getComments(email)).thenReturn(DeferredK.just(comments))
        `when`(mockCommentMapper.toComment(anyOrNull())).thenReturn(mockComment)

        val result = sut.getComments(email)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(arrayListOf(mockComment, mockComment), it) })
        verify(mockCommentsNetServiceApi).getComments(email)
    }

    @Test
    fun `getComments should emmit error when unsuccessful`() {
        val email = "email"
        val error = Exception("error")
        `when`(mockCommentsNetServiceApi.getComments(email)).thenReturn(DeferredK.raiseError(error))
        `when`(mockCommentMapper.toComment(anyOrNull())).thenReturn(mockComment)

        val result = sut.getComments(email)
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockCommentsNetServiceApi).getComments(email)
    }
}