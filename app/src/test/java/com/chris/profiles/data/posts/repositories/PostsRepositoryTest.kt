package com.chris.profiles.data.posts.repositories

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.deferredk.monadDefer.monadDefer
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.data.posts.repositories.mappers.PostMapper
import com.chris.profiles.datasources.network.posts.netsources.PostsNetServiceApi
import com.chris.profiles.datasources.network.posts.netsources.models.PostNet
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.posts.models.Post
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class PostsRepositoryTest {
    private val monadDefer = DeferredK.monadDefer()
    @Mock
    private lateinit var mockPostsNetServiceApi: PostsNetServiceApi<ForDeferredK>
    @Mock
    private lateinit var mockPostNet: PostNet
    @Mock
    private lateinit var mockPost: Post
    @Mock
    private lateinit var mockPostMapper: PostMapper

    private lateinit var sut: PostsRepositoryApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = PostsRepository(mockPostsNetServiceApi, monadDefer, mockPostMapper)
    }

    @Test
    fun `getPost should return Post when successful`() {
        val postId = 1
        `when`(mockPostsNetServiceApi.getPost(postId)).thenReturn(DeferredK.just(mockPostNet))
        `when`(mockPostMapper.toDomainPost(anyOrNull())).thenReturn(mockPost)

        val result = sut.getPost(postId)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockPost, it) })
        verify(mockPostsNetServiceApi).getPost(postId)
    }

    @Test
    fun `getPost should emmit error when unsuccessful`() {
        val postId = 1
        val error = Exception("error")
        `when`(mockPostsNetServiceApi.getPost(postId)).thenReturn(DeferredK.raiseError(error))
        `when`(mockPostMapper.toDomainPost(anyOrNull())).thenReturn(mockPost)

        val result = sut.getPost(postId)
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockPostsNetServiceApi).getPost(postId)
    }

    @Test
    fun `getPosts should return Posts when successful`() {
        val posts = arrayListOf(mockPostNet, mockPostNet)
        `when`(mockPostsNetServiceApi.getPosts()).thenReturn(DeferredK.just(posts))
        `when`(mockPostMapper.toDomainPost(anyOrNull())).thenReturn(mockPost)

        val result = sut.getPosts()
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(arrayListOf(mockPost, mockPost), it) })
        verify(mockPostsNetServiceApi).getPosts()
    }

    @Test
    fun `getPosts should emmit error when unsuccessful`() {
        val error = Exception("error")
        `when`(mockPostsNetServiceApi.getPosts()).thenReturn(DeferredK.raiseError(error))
        `when`(mockPostMapper.toDomainPost(anyOrNull())).thenReturn(mockPost)

        val result = sut.getPosts()
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockPostsNetServiceApi).getPosts()
    }
}