package com.chris.profiles.data.users.repositories

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.deferredk.monadDefer.monadDefer
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.data.users.repositories.mappers.UserMapper
import com.chris.profiles.datasources.network.users.netsources.UsersNetServiceApi
import com.chris.profiles.datasources.network.users.netsources.models.UserNet
import com.chris.profiles.domain.users.UsersRepositoryApi
import com.chris.profiles.domain.users.models.User
import com.nhaarman.mockitokotlin2.anyOrNull
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class UsersRepositoryTest {
    private val monadDefer = DeferredK.monadDefer()
    @Mock
    private lateinit var mockUsersNetServiceApi: UsersNetServiceApi<ForDeferredK>
    @Mock
    private lateinit var mockUserNet: UserNet
    @Mock
    private lateinit var mockUser: User
    @Mock
    private lateinit var userMapper: UserMapper

    private lateinit var sut: UsersRepositoryApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = UsersRepository(mockUsersNetServiceApi, monadDefer, userMapper)
    }

    @Test
    fun `getUser should return User when successful`() {
        val userId = 1
        `when`(mockUsersNetServiceApi.getUser(userId)).thenReturn(DeferredK.just(mockUserNet))
        `when`(userMapper.toDomainUser(anyOrNull())).thenReturn(mockUser)

        val result = sut.getUser(userId)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockUser, it) })
        verify(mockUsersNetServiceApi).getUser(userId)
    }

    @Test
    fun `getUser should emmit error when unsuccessful`() {
        val userId = 1
        val error = Exception("error")
        `when`(mockUsersNetServiceApi.getUser(userId)).thenReturn(DeferredK.raiseError(error))
        `when`(userMapper.toDomainUser(anyOrNull())).thenReturn(mockUser)

        val result = sut.getUser(userId)
            .unsafeAttemptSync()

        result.fold({ assertEquals(error.localizedMessage, it.localizedMessage) }, { })
        verify(mockUsersNetServiceApi).getUser(userId)
    }
}