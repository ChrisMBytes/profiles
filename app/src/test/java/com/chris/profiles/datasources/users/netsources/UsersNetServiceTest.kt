package com.chris.profiles.datasources.users.netsources

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.deferredk.monadDefer.monadDefer
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.datasources.network.users.UsersServiceApi
import com.chris.profiles.datasources.network.users.netsources.UsersNetService
import com.chris.profiles.datasources.network.users.netsources.UsersNetServiceApi
import com.chris.profiles.datasources.network.users.netsources.models.UserNet
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.mock.Calls

class UsersNetServiceTest {
    private val monadDefer = DeferredK.monadDefer()
    @Mock
    private lateinit var mockUsersServiceApi: UsersServiceApi
    @Mock
    private lateinit var mockCallListUserNet: Call<List<UserNet>>

    private lateinit var sut: UsersNetServiceApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = UsersNetService(mockUsersServiceApi, monadDefer)
    }

    @Test
    fun `getUser should return UserNext when rest call is successful`() {
        val userId = 1
        `when`(mockUsersServiceApi.getUser(userId)).thenReturn(mockCallListUserNet)

        val result = sut.getUser(userId)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockCallListUserNet, it) })
        verify(mockUsersServiceApi).getUser(userId)
    }

    @Test
    fun `getUser should return error when rest call is unsuccessful`() {
        val userId = 1
        val exception = Exception("error")
        `when`(mockUsersServiceApi.getUser(userId)).thenReturn(Calls.failure(exception))

        val result = sut.getUser(userId)
            .unsafeAttemptSync()

        result.fold({ assertEquals(exception.localizedMessage, it.localizedMessage) }, {})
        verify(mockUsersServiceApi).getUser(userId)
    }
}