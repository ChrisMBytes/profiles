package com.chris.profiles.datasources.posts.netsources

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.deferredk.monadDefer.monadDefer
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.datasources.network.posts.PostsServiceApi
import com.chris.profiles.datasources.network.posts.netsources.PostsNetService
import com.chris.profiles.datasources.network.posts.netsources.PostsNetServiceApi
import com.chris.profiles.datasources.network.posts.netsources.models.PostNet
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.mock.Calls

class PostsNetServiceTest {
    private val monadDefer = DeferredK.monadDefer()
    @Mock
    private lateinit var mockPostsServiceApi: PostsServiceApi
    @Mock
    private lateinit var mockCallListPostNet: Call<List<PostNet>>

    private lateinit var sut: PostsNetServiceApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = PostsNetService(mockPostsServiceApi, monadDefer)
    }

    @Test
    fun `getPost should return PostNext when rest call is successful`() {
        val postId = 1
        `when`(mockPostsServiceApi.getPost(postId)).thenReturn(mockCallListPostNet)

        val result = sut.getPost(postId)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockCallListPostNet, it) })
        verify(mockPostsServiceApi).getPost(postId)
    }

    @Test
    fun `getPost should return error when rest call is unsuccessful`() {
        val postId = 1
        val exception = Exception("error")
        `when`(mockPostsServiceApi.getPost(postId)).thenReturn(Calls.failure(exception))

        val result = sut.getPost(postId)
            .unsafeAttemptSync()

        result.fold({ assertEquals(exception.localizedMessage, it.localizedMessage) }, {})
        verify(mockPostsServiceApi).getPost(postId)
    }

    @Test
    fun `getPosts should return PostNext when rest call is successful`() {
        `when`(mockPostsServiceApi.getPosts()).thenReturn(mockCallListPostNet)

        val result = sut.getPosts()
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockCallListPostNet, it) })
        verify(mockPostsServiceApi).getPosts()
    }

    @Test
    fun `getPosts should return error when rest call is unsuccessful`() {
        val exception = Exception("error")
        `when`(mockPostsServiceApi.getPosts()).thenReturn(Calls.failure(exception))

        val result = sut.getPosts()
            .unsafeAttemptSync()

        result.fold({ assertEquals(exception.localizedMessage, it.localizedMessage) }, {})
        verify(mockPostsServiceApi).getPosts()
    }
}