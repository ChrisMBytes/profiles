package com.chris.profiles.datasources.comments.netsources

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.deferredk.monadDefer.monadDefer
import arrow.effects.unsafeAttemptSync
import com.chris.profiles.datasources.network.comments.CommentsServiceApi
import com.chris.profiles.datasources.network.comments.netsources.CommentsNetService
import com.chris.profiles.datasources.network.comments.netsources.CommentsNetServiceApi
import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.mock.Calls

class CommentsNetServiceTest {
    private val monadDefer = DeferredK.monadDefer()
    @Mock
    private lateinit var mockCommentsServiceApi: CommentsServiceApi
    @Mock
    private lateinit var mockCallListCommentNet: Call<List<CommentNet>>

    private lateinit var sut: CommentsNetServiceApi<ForDeferredK>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = CommentsNetService(mockCommentsServiceApi, monadDefer)
    }

    @Test
    fun `getComments should return CommentNet when rest call is successful`() {
        val email = "email"
        `when`(mockCommentsServiceApi.getComments(email)).thenReturn(mockCallListCommentNet)

        val result = sut.getComments(email)
            .unsafeAttemptSync()

        result.fold({}, { assertEquals(mockCallListCommentNet, it) })
        verify(mockCommentsServiceApi).getComments(email)
    }

    @Test
    fun `getComments should return error when rest call is unsuccessful`() {
        val email = "email"
        val exception = Exception("error")
        `when`(mockCommentsServiceApi.getComments(email)).thenReturn(Calls.failure(exception))

        val result = sut.getComments(email)
            .unsafeAttemptSync()

        result.fold({ assertEquals(exception.localizedMessage, it.localizedMessage) }, {})
        verify(mockCommentsServiceApi).getComments(email)
    }
}