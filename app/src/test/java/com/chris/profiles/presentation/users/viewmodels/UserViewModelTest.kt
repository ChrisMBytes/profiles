package com.chris.profiles.presentation.users.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import com.chris.profiles.domain.users.interactors.GetUserApi
import com.chris.profiles.domain.users.models.User
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class UserViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var mockGetUserApi: GetUserApi<ForDeferredK>
    @Mock
    private lateinit var mockUser: User

    private lateinit var sut: UserViewModelApi

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = UserViewModel(mockGetUserApi, Dispatchers.Unconfined)
    }

    @Test
    fun `initUser should assign name and email when call is successful`() {
        val userId = 1
        val name = "name"
        val email = "email"
        `when`(mockGetUserApi.getUser(userId)).thenReturn(DeferredK.just(mockUser))
        `when`(mockUser.name).thenReturn(name)
        `when`(mockUser.email).thenReturn(email)

        sut.initUser(userId)

        assertEquals(name, sut.name.value)
        assertEquals(email, sut.email.value)
        verify(mockGetUserApi).getUser(userId)
    }

    @Test
    fun `initUser should emmit error when call is unsuccessful`() {
        val userId = 1
        val errorMessage = "error"
        `when`(mockGetUserApi.getUser(userId)).thenReturn(DeferredK.raiseError(Exception(errorMessage)))

        sut.initUser(userId)

        assertEquals(errorMessage, sut.userError.value)
        verify(mockGetUserApi).getUser(userId)
    }
}