package com.chris.profiles.presentation.recyclerview.posts.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chris.profiles.domain.posts.models.Post
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class PostItemViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var mockPost: Post

    private val sut = PostItemViewModel()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `setItem should set values`() {
        val userId = 1
        val id = 1
        val title = "title"
        val body = "body"
        `when`(mockPost.userId).thenReturn(userId)
        `when`(mockPost.id).thenReturn(id)
        `when`(mockPost.title).thenReturn(title)
        `when`(mockPost.body).thenReturn(body)

        sut.setItem(mockPost)

        assertEquals(userId, sut.userId.value)
        assertEquals(id, sut.id.value)
        assertEquals(title, sut.title.value)
        assertEquals(body, sut.body.value)
    }

    @Test
    fun `postValue should set values`() {
        val userId = 1
        val id = 1
        val title = "title"
        val body = "body"
        `when`(mockPost.userId).thenReturn(userId)
        `when`(mockPost.id).thenReturn(id)
        `when`(mockPost.title).thenReturn(title)
        `when`(mockPost.body).thenReturn(body)

        sut.postValue(mockPost)

        assertEquals(userId, sut.userId.value)
        assertEquals(id, sut.id.value)
        assertEquals(title, sut.title.value)
        assertEquals(body, sut.body.value)
    }
}