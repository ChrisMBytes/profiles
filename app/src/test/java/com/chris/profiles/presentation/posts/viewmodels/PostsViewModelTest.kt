package com.chris.profiles.presentation.posts.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import com.chris.profiles.domain.posts.interactors.GetPostsApi
import com.chris.profiles.domain.posts.models.Post
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class PostsViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var mockGetPostsApi: GetPostsApi<ForDeferredK>
    @Mock
    private lateinit var mockPost: Post

    private lateinit var sut: PostsViewModelApi

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `init should assign posts when call is successful`() {
        val posts = arrayListOf(mockPost, mockPost)
        `when`(mockGetPostsApi.getPosts()).thenReturn(DeferredK.just(posts))

        sut = PostsViewModel(mockGetPostsApi, Dispatchers.Unconfined)

        assertEquals(posts, sut.posts.value)
        verify(mockGetPostsApi).getPosts()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `init should emmit error when call is unsuccessful`() {
        val errorMessage = "error"
        `when`(mockGetPostsApi.getPosts()).thenReturn(DeferredK.raiseError(Exception(errorMessage)))

        sut = PostsViewModel(mockGetPostsApi, Dispatchers.Unconfined)

        assertEquals(errorMessage, sut.error.value)
        verify(mockGetPostsApi).getPosts()
    }
}