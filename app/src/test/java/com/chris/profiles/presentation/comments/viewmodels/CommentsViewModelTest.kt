package com.chris.profiles.presentation.comments.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import com.chris.profiles.domain.comments.interactors.GetCommentsApi
import com.chris.profiles.domain.comments.models.Comment
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CommentsViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var mockGetCommentsApi: GetCommentsApi<ForDeferredK>
    @Mock
    private lateinit var mockComment: Comment

    private lateinit var sut: CommentsViewModelApi

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = CommentsViewModel(mockGetCommentsApi, Dispatchers.Unconfined)
    }

    @Test
    fun `initComments should assign total when call is successful`() {
        val email = "email"
        val comments = arrayListOf(mockComment, mockComment)
        `when`(mockGetCommentsApi.getComments(email)).thenReturn(DeferredK.just(comments))

        sut.initComments(email)

        assertEquals("2", sut.total.value)
        verify(mockGetCommentsApi).getComments(email)
    }

    @Test
    fun `initComments should emmit error when call is unsuccessful`() {
        val email = "email"
        val errorMessage = "error"
        `when`(mockGetCommentsApi.getComments(email)).thenReturn(DeferredK.raiseError(Exception(errorMessage)))

        sut.initComments(email)

        assertEquals(errorMessage, sut.commentsError.value)
        verify(mockGetCommentsApi).getComments(email)
    }
}