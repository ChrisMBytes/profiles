package com.chris.profiles.presentation.posts.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import com.chris.profiles.domain.posts.interactors.GetPostApi
import com.chris.profiles.domain.posts.models.Post
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModelApi
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class UserPostViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var mockGetPostApi: GetPostApi<ForDeferredK>
    @Mock
    private lateinit var mockPostItemViewModelApi: PostItemViewModelApi
    @Mock
    private lateinit var mockPost: Post
    @Mock
    private lateinit var mockUserIdLiveDataInt: MutableLiveData<Int>
    @Mock
    private lateinit var mockIdLiveDataInt: MutableLiveData<Int>
    @Mock
    private lateinit var mockTitleLiveDataString: MutableLiveData<String>
    @Mock
    private lateinit var mockBodyLiveDataString: MutableLiveData<String>

    private lateinit var sut: UserPostViewModelApi

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        sut = UserPostViewModel(mockPostItemViewModelApi, mockGetPostApi, Dispatchers.Unconfined)
    }

    @Test
    fun `initUserPost should assign userId, id, title and body when call is successful`() {
        val postId = 1
        val userId = 1
        val id = 1
        val title = "title"
        val body = "body"
        `when`(mockPost.userId).thenReturn(userId)
        `when`(mockPostItemViewModelApi.userId).thenReturn(mockUserIdLiveDataInt)
        `when`(mockUserIdLiveDataInt.value).thenReturn(userId)
        `when`(mockPost.id).thenReturn(id)
        `when`(mockPostItemViewModelApi.id).thenReturn(mockIdLiveDataInt)
        `when`(mockIdLiveDataInt.value).thenReturn(id)
        `when`(mockPost.title).thenReturn(title)
        `when`(mockPostItemViewModelApi.title).thenReturn(mockTitleLiveDataString)
        `when`(mockTitleLiveDataString.value).thenReturn(title)
        `when`(mockPost.body).thenReturn(body)
        `when`(mockPostItemViewModelApi.body).thenReturn(mockBodyLiveDataString)
        `when`(mockBodyLiveDataString.value).thenReturn(body)
        `when`(mockGetPostApi.getPost(postId)).thenReturn(DeferredK.just(mockPost))

        sut.initUserPost(postId)

        assertEquals(userId, sut.userId.value)
        assertEquals(id, sut.id.value)
        assertEquals(title, sut.title.value)
        assertEquals(body, sut.body.value)
        verify(mockGetPostApi).getPost(postId)
    }

    @Test
    fun `initUserPost should emmit error when call is unsuccessful`() {
        val postId = 1
        val errorMessage = "error"
        `when`(mockGetPostApi.getPost(postId)).thenReturn(DeferredK.raiseError(Exception(errorMessage)))

        sut.initUserPost(postId)

        assertEquals(errorMessage, sut.userPostError.value)
        verify(mockGetPostApi).getPost(postId)
    }
}