package com.chris.profiles.presentation.home.viewmodels

import com.chris.profiles.presentation.posts.viewmodels.PostsViewModelApi

class HomeViewModel(
    private val postsViewModelApi: PostsViewModelApi
) : HomeViewModelApi, PostsViewModelApi by postsViewModelApi