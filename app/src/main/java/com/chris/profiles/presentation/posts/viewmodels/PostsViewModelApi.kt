package com.chris.profiles.presentation.posts.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.profiles.domain.posts.models.Post
import com.chris.profiles.presentation.viewmodels.StateViewModel

interface PostsViewModelApi : StateViewModel {
    val posts: MutableLiveData<List<Post>>
    val error: MutableLiveData<String>
}