package com.chris.profiles.presentation.users.viewmodels

import androidx.lifecycle.MutableLiveData
import arrow.effects.ForDeferredK
import arrow.effects.unsafeRunAsync
import com.chris.profiles.domain.users.interactors.GetUserApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.CoroutineContext

class UserViewModel(
    private val getUserApi: GetUserApi<ForDeferredK>,
    private val context: CoroutineContext = Dispatchers.Default
) : UserViewModelApi {
    override val name = MutableLiveData<String>()
    override val email = MutableLiveData<String>()
    override val userError = MutableLiveData<String>()

    override fun initUser(userId: Int) {
        runBlocking(context) {
            getUserApi.getUser(userId)
                .unsafeRunAsync { either ->
                    either.fold(
                        { userError.postValue(it.localizedMessage) },
                        {
                            name.postValue(it.name)
                            email.postValue(it.email)
                        }
                    )
                }
        }
    }

    override fun clear() {
        context.cancel()
    }
}