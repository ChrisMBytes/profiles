package com.chris.profiles.presentation.posts.viewmodels

import androidx.lifecycle.MutableLiveData
import arrow.effects.ForDeferredK
import arrow.effects.unsafeRunAsync
import com.chris.profiles.domain.posts.interactors.GetPostsApi
import com.chris.profiles.domain.posts.models.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.CoroutineContext

class PostsViewModel(
    private val getPostsApi: GetPostsApi<ForDeferredK>,
    private val context: CoroutineContext = Dispatchers.Default
) : PostsViewModelApi {
    override val posts = MutableLiveData<List<Post>>()
    override val error = MutableLiveData<String>()

    init {
        runBlocking(context) {
            getPostsApi.getPosts()
                .unsafeRunAsync { either ->
                    either.fold(
                        { error.postValue(it.localizedMessage) },
                        { posts.postValue(it) }
                    )
                }
        }
    }

    override fun clear() {
        context.cancel()
    }
}