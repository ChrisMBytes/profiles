package com.chris.profiles.presentation.posts.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModelApi
import com.chris.profiles.presentation.viewmodels.StateViewModel

interface UserPostViewModelApi : StateViewModel, PostItemViewModelApi {
    val userPostError: MutableLiveData<String>
    fun initUserPost(postId: Int)
}