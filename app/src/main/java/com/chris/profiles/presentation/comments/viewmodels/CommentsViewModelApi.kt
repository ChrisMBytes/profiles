package com.chris.profiles.presentation.comments.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.profiles.presentation.viewmodels.StateViewModel

interface CommentsViewModelApi : StateViewModel {
    val total: MutableLiveData<String>
    val commentsError: MutableLiveData<String>
    fun initComments(email: String)
}