package com.chris.profiles.presentation.recyclerview.viewmodels

interface ItemViewModel<ITEM_T> {

    fun setItem(item: ITEM_T)
}
