package com.chris.profiles.presentation.home.viewmodels

import com.chris.profiles.presentation.posts.viewmodels.PostsViewModelApi

interface HomeViewModelApi : PostsViewModelApi