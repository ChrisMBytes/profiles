package com.chris.profiles.presentation.posts.viewmodels

import androidx.lifecycle.MutableLiveData
import arrow.effects.ForDeferredK
import arrow.effects.unsafeRunAsync
import com.chris.profiles.domain.posts.interactors.GetPostApi
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModelApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.CoroutineContext

class UserPostViewModel(
    postItemViewModelApi: PostItemViewModelApi,
    private val getPostApi: GetPostApi<ForDeferredK>,
    private val context: CoroutineContext = Dispatchers.Default
) : UserPostViewModelApi, PostItemViewModelApi by postItemViewModelApi {
    override val userPostError = MutableLiveData<String>()

    override fun initUserPost(postId: Int) {
        runBlocking(context) {
            getPostApi.getPost(postId)
                .unsafeRunAsync { either ->
                    either.fold(
                        { userPostError.postValue(it.localizedMessage) },
                        { postValue(it) }
                    )
                }
        }
    }

    override fun clear() {
        context.cancel()
    }
}