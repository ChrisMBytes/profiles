package com.chris.profiles.presentation.recyclerview.posts.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.profiles.domain.posts.models.Post

class PostItemViewModel : PostItemViewModelApi {
    override val userId = MutableLiveData<Int>()
    override val id = MutableLiveData<Int>()
    override val title = MutableLiveData<String>()
    override val body = MutableLiveData<String>()

    override fun setItem(item: Post) {
        userId.value = item.userId
        id.value = item.id
        title.value = item.title
        body.value = item.body
    }

    override fun postValue(item: Post) {
        userId.postValue(item.userId)
        id.postValue(item.id)
        title.postValue(item.title)
        body.postValue(item.body)
    }
}