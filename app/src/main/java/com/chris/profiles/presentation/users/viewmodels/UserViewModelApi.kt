package com.chris.profiles.presentation.users.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.profiles.presentation.viewmodels.StateViewModel

interface UserViewModelApi : StateViewModel {
    val name: MutableLiveData<String>
    val email: MutableLiveData<String>
    val userError: MutableLiveData<String>
    fun initUser(userId: Int)
}