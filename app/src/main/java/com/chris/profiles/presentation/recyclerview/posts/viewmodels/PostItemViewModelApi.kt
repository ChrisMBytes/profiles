package com.chris.profiles.presentation.recyclerview.posts.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.profiles.domain.posts.models.Post
import com.chris.profiles.presentation.recyclerview.viewmodels.ItemViewModel

interface PostItemViewModelApi : ItemViewModel<Post> {
    val userId: MutableLiveData<Int>
    val id: MutableLiveData<Int>
    val title: MutableLiveData<String>
    val body: MutableLiveData<String>
    fun postValue(item: Post)
}