package com.chris.profiles.presentation.postprofile.viewmodels

import com.chris.profiles.presentation.comments.viewmodels.CommentsViewModelApi
import com.chris.profiles.presentation.posts.viewmodels.UserPostViewModelApi
import com.chris.profiles.presentation.users.viewmodels.UserViewModelApi
import com.chris.profiles.presentation.viewmodels.StateViewModel

interface PostProfileViewModelApi : StateViewModel, UserPostViewModelApi, UserViewModelApi, CommentsViewModelApi