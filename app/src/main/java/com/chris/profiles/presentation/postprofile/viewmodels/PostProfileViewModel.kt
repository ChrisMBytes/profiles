package com.chris.profiles.presentation.postprofile.viewmodels

import com.chris.profiles.presentation.comments.viewmodels.CommentsViewModelApi
import com.chris.profiles.presentation.posts.viewmodels.UserPostViewModelApi
import com.chris.profiles.presentation.users.viewmodels.UserViewModelApi

class PostProfileViewModel(
    private val userPostViewModelApi: UserPostViewModelApi,
    private val userViewModelApi: UserViewModelApi,
    private val commentsViewModelApi: CommentsViewModelApi
) : PostProfileViewModelApi,
    UserPostViewModelApi by userPostViewModelApi,
    UserViewModelApi by userViewModelApi,
    CommentsViewModelApi by commentsViewModelApi {

    override fun clear() {
        userPostViewModelApi.clear()
        userViewModelApi.clear()
        commentsViewModelApi.clear()
    }
}