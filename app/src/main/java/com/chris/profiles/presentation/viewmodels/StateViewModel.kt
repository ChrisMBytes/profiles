package com.chris.profiles.presentation.viewmodels

interface StateViewModel {
    fun clear()
}