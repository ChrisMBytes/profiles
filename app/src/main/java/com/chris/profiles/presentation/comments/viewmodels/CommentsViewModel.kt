package com.chris.profiles.presentation.comments.viewmodels

import androidx.lifecycle.MutableLiveData
import arrow.effects.ForDeferredK
import arrow.effects.unsafeRunAsync
import com.chris.profiles.domain.comments.interactors.GetCommentsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.CoroutineContext

class CommentsViewModel(
    private val getCommentsApi: GetCommentsApi<ForDeferredK>,
    private val context: CoroutineContext = Dispatchers.Default
) : CommentsViewModelApi {
    override val total = MutableLiveData<String>()
    override val commentsError = MutableLiveData<String>()

    override fun initComments(email: String) {
        runBlocking(context) {
            getCommentsApi.getComments(email)
                .unsafeRunAsync { either ->
                    either.fold(
                        { commentsError.postValue(it.localizedMessage) },
                        { total.postValue(it.size.toString()) }
                    )
                }
        }
    }

    override fun clear() {
        context.cancel()
    }
}