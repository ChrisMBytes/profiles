package com.chris.profiles.di

import android.app.Application
import com.chris.profiles.view.ProfilesApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        PresentationModule::class,
        DomainModule::class,
        DataModule::class,
        DataSourcesModule::class
    ]
)
interface AppComponent : AndroidInjector<ProfilesApplication> {
    override fun inject(instance: ProfilesApplication)

    @Component.Builder
    interface Builder {
        fun dataSourcesModule(dataSourcesModule: DataSourcesModule): Builder

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}