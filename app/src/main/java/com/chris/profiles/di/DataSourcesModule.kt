package com.chris.profiles.di

import arrow.effects.DeferredK
import arrow.effects.ForDeferredK
import arrow.effects.deferredk.async.async
import arrow.effects.deferredk.monadDefer.monadDefer
import arrow.effects.typeclasses.Async
import arrow.effects.typeclasses.MonadDefer
import com.chris.profiles.datasources.network.ProfilesConfig
import com.chris.profiles.datasources.network.RetrofitServiceGenerator
import com.chris.profiles.datasources.network.comments.CommentsServiceApi
import com.chris.profiles.datasources.network.comments.netsources.CommentsNetService
import com.chris.profiles.datasources.network.comments.netsources.CommentsNetServiceApi
import com.chris.profiles.datasources.network.posts.PostsServiceApi
import com.chris.profiles.datasources.network.posts.netsources.PostsNetService
import com.chris.profiles.datasources.network.posts.netsources.PostsNetServiceApi
import com.chris.profiles.datasources.network.users.UsersServiceApi
import com.chris.profiles.datasources.network.users.netsources.UsersNetService
import com.chris.profiles.datasources.network.users.netsources.UsersNetServiceApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataSourcesModule {

    // Posts
    @Provides
    fun providesPostsNetServiceApi(
        postsServiceApi: PostsServiceApi,
        async: Async<ForDeferredK>
    ): PostsNetServiceApi<ForDeferredK> {
        return PostsNetService(postsServiceApi, async)
    }

    @Singleton
    @Provides
    fun providesPostsServiceApi(profilesConfig: ProfilesConfig): PostsServiceApi {
        return RetrofitServiceGenerator(profilesConfig).createService(PostsServiceApi::class.java)
    }
    //

    // Users
    @Provides
    fun providesUsersNetServiceApi(
        usersServiceApi: UsersServiceApi,
        async: Async<ForDeferredK>
    ): UsersNetServiceApi<ForDeferredK> {
        return UsersNetService(usersServiceApi, async)
    }

    @Singleton
    @Provides
    fun providesUsersServiceApi(profilesConfig: ProfilesConfig): UsersServiceApi {
        return RetrofitServiceGenerator(profilesConfig).createService(UsersServiceApi::class.java)
    }
    //

    // Comments
    @Provides
    fun providesCommentsNetServiceApi(
        commentsServiceApi: CommentsServiceApi,
        async: Async<ForDeferredK>
    ): CommentsNetServiceApi<ForDeferredK> {
        return CommentsNetService(commentsServiceApi, async)
    }

    @Singleton
    @Provides
    fun providesCommentsServiceApi(profilesConfig: ProfilesConfig): CommentsServiceApi {
        return RetrofitServiceGenerator(profilesConfig).createService(CommentsServiceApi::class.java)
    }
    //

    @Provides
    fun providesProfilesConfig(): ProfilesConfig {
        return ProfilesConfig()
    }

    @Provides
    fun providesAsyncDeferred(): Async<ForDeferredK> {
        return DeferredK.async()
    }

    @Provides
    fun providesMonadDeferDeferred(): MonadDefer<ForDeferredK> {
        return DeferredK.monadDefer()
    }
}