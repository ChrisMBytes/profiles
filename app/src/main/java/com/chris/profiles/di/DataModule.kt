package com.chris.profiles.di

import arrow.effects.ForDeferredK
import arrow.effects.typeclasses.Async
import com.chris.profiles.data.comments.repositories.CommentsRepository
import com.chris.profiles.data.posts.repositories.PostsRepository
import com.chris.profiles.data.users.repositories.UsersRepository
import com.chris.profiles.datasources.network.comments.netsources.CommentsNetServiceApi
import com.chris.profiles.datasources.network.posts.netsources.PostsNetServiceApi
import com.chris.profiles.datasources.network.users.netsources.UsersNetServiceApi
import com.chris.profiles.domain.comments.CommentsRepositoryApi
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.users.UsersRepositoryApi
import dagger.Module
import dagger.Provides

@Module
class DataModule {

    // Posts
    @Provides
    fun providesPostsRepositoryApi(
        postsNetServiceApi: PostsNetServiceApi<ForDeferredK>,
        async: Async<ForDeferredK>
    ): PostsRepositoryApi<ForDeferredK> {
        return PostsRepository(postsNetServiceApi, async)
    }
    //

    // Users
    @Provides
    fun providesUsersRepositoryApi(
        usersNetServiceApi: UsersNetServiceApi<ForDeferredK>,
        async: Async<ForDeferredK>
    ): UsersRepositoryApi<ForDeferredK> {
        return UsersRepository(usersNetServiceApi, async)
    }
    //

    // Comments
    @Provides
    fun providesCommentsRepositoryApi(
        commentsNetServiceApi: CommentsNetServiceApi<ForDeferredK>,
        async: Async<ForDeferredK>
    ): CommentsRepositoryApi<ForDeferredK> {
        return CommentsRepository(commentsNetServiceApi, async)
    }
    //
}