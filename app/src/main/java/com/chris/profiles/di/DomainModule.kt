package com.chris.profiles.di

import arrow.effects.ForDeferredK
import com.chris.profiles.domain.comments.CommentsRepositoryApi
import com.chris.profiles.domain.comments.interactors.GetComments
import com.chris.profiles.domain.comments.interactors.GetCommentsApi
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.posts.interactors.GetPost
import com.chris.profiles.domain.posts.interactors.GetPostApi
import com.chris.profiles.domain.posts.interactors.GetPosts
import com.chris.profiles.domain.posts.interactors.GetPostsApi
import com.chris.profiles.domain.users.UsersRepositoryApi
import com.chris.profiles.domain.users.interactors.GetUser
import com.chris.profiles.domain.users.interactors.GetUserApi
import dagger.Module
import dagger.Provides

@Module
class DomainModule {

    // Posts
    @Provides
    fun providesGetPostsApi(repositoryApi: PostsRepositoryApi<ForDeferredK>): GetPostsApi<ForDeferredK> {
        return GetPosts(repositoryApi)
    }

    @Provides
    fun providesGetPostApi(repositoryApi: PostsRepositoryApi<ForDeferredK>): GetPostApi<ForDeferredK> {
        return GetPost(repositoryApi)
    }

    // Users
    @Provides
    fun providesGetUserApi(repositoryApi: UsersRepositoryApi<ForDeferredK>): GetUserApi<ForDeferredK> {
        return GetUser(repositoryApi)
    }

    // Comments
    @Provides
    fun providesGetCommentsApi(repositoryApi: CommentsRepositoryApi<ForDeferredK>): GetCommentsApi<ForDeferredK> {
        return GetComments(repositoryApi)
    }
}