package com.chris.profiles.di

import arrow.effects.ForDeferredK
import com.chris.profiles.domain.comments.interactors.GetCommentsApi
import com.chris.profiles.domain.posts.interactors.GetPostApi
import com.chris.profiles.domain.posts.interactors.GetPostsApi
import com.chris.profiles.domain.users.interactors.GetUserApi
import com.chris.profiles.presentation.comments.viewmodels.CommentsViewModel
import com.chris.profiles.presentation.comments.viewmodels.CommentsViewModelApi
import com.chris.profiles.presentation.home.viewmodels.HomeViewModel
import com.chris.profiles.presentation.home.viewmodels.HomeViewModelApi
import com.chris.profiles.presentation.postprofile.viewmodels.PostProfileViewModel
import com.chris.profiles.presentation.postprofile.viewmodels.PostProfileViewModelApi
import com.chris.profiles.presentation.posts.viewmodels.PostsViewModel
import com.chris.profiles.presentation.posts.viewmodels.PostsViewModelApi
import com.chris.profiles.presentation.posts.viewmodels.UserPostViewModel
import com.chris.profiles.presentation.posts.viewmodels.UserPostViewModelApi
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModel
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModelApi
import com.chris.profiles.presentation.users.viewmodels.UserViewModel
import com.chris.profiles.presentation.users.viewmodels.UserViewModelApi
import dagger.Module
import dagger.Provides

@Module
class PresentationModule {

    // Home
    @Provides
    fun providesHomeViewModelApi(postsViewModelApi: PostsViewModelApi): HomeViewModelApi {
        return HomeViewModel(postsViewModelApi)
    }

    // PostProfile
    @Provides
    fun providesPostProfileViewModelApi(
        userPostViewModelApi: UserPostViewModelApi,
        userViewModelApi: UserViewModelApi,
        commentsViewModelApi: CommentsViewModelApi
    ): PostProfileViewModelApi {
        return PostProfileViewModel(userPostViewModelApi, userViewModelApi, commentsViewModelApi)
    }

    // Posts
    @Provides
    fun providesPostsViewModelApi(getPostsApi: GetPostsApi<ForDeferredK>): PostsViewModelApi {
        return PostsViewModel(getPostsApi)
    }

    @Provides
    fun providesUserPostViewModelApi(
        postItemViewModelApi: PostItemViewModelApi,
        getPostApi: GetPostApi<ForDeferredK>
    ): UserPostViewModelApi {
        return UserPostViewModel(postItemViewModelApi, getPostApi)
    }

    @Provides
    fun providesPostItemViewModelApi(): PostItemViewModelApi {
        return PostItemViewModel()
    }

    // User
    @Provides
    fun providesUserViewModelApi(getUserApi: GetUserApi<ForDeferredK>): UserViewModelApi {
        return UserViewModel(getUserApi)
    }

    // Comments
    @Provides
    fun providesCommentsViewModelApi(getCommentsApi: GetCommentsApi<ForDeferredK>): CommentsViewModelApi {
        return CommentsViewModel(getCommentsApi)
    }
}