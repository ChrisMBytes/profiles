package com.chris.profiles.di

import com.chris.profiles.view.home.HomeActivity
import com.chris.profiles.view.postprofile.PostProfileActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun bindPostProfileActivity(): PostProfileActivity
}
