package com.chris.profiles.data.users.repositories.mappers

import com.chris.profiles.datasources.network.users.netsources.models.AddressNet
import com.chris.profiles.datasources.network.users.netsources.models.CompanyNet
import com.chris.profiles.datasources.network.users.netsources.models.UserNet
import com.chris.profiles.domain.users.models.User

object UserMapper {
    fun toDomainUser(userNet: UserNet): User {
        return with(userNet) {
            User(
                AddressMapper.toDomainAddress(address ?: AddressNet()),
                CompanyMapper.toDomainCompany(company ?: CompanyNet()),
                email ?: "",
                id ?: 0,
                name ?: "",
                phone ?: "",
                username ?: "",
                website ?: ""
            )
        }
    }
}