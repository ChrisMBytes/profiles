package com.chris.profiles.data.users.repositories.mappers

import com.chris.profiles.datasources.network.users.netsources.models.GeoNet
import com.chris.profiles.domain.users.models.Geo

object GeoMapper {
    fun toDomainGeo(geoNet: GeoNet): Geo {
        return with(geoNet) {
            Geo(lat ?: "", lng ?: "")
        }
    }
}