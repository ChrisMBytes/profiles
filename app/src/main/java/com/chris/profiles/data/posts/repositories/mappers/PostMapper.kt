package com.chris.profiles.data.posts.repositories.mappers

import com.chris.profiles.datasources.network.posts.netsources.models.PostNet
import com.chris.profiles.domain.posts.models.Post

object PostMapper {
    fun toDomainPost(postNet: PostNet): Post {
        return with(postNet) {
            Post(
                userId ?: 0,
                id ?: 0,
                title ?: "",
                body ?: ""
            )
        }
    }
}