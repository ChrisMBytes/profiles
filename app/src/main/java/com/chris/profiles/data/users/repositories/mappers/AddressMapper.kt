package com.chris.profiles.data.users.repositories.mappers

import com.chris.profiles.datasources.network.users.netsources.models.AddressNet
import com.chris.profiles.datasources.network.users.netsources.models.GeoNet
import com.chris.profiles.domain.users.models.Address

object AddressMapper {
    fun toDomainAddress(addressNet: AddressNet): Address {
        return with(addressNet) {
            Address(
                city ?: "",
                GeoMapper.toDomainGeo(geo ?: GeoNet()),
                street ?: "",
                suite ?: "",
                zipcode ?: ""
            )
        }
    }
}