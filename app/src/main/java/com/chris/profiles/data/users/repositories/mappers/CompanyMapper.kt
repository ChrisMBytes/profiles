package com.chris.profiles.data.users.repositories.mappers

import com.chris.profiles.datasources.network.users.netsources.models.CompanyNet
import com.chris.profiles.domain.users.models.Company

object CompanyMapper {
    fun toDomainCompany(companyNet: CompanyNet): Company {
        return with(companyNet) {
            Company(bs ?: "", catchPhrase ?: "", name ?: "")
        }
    }
}