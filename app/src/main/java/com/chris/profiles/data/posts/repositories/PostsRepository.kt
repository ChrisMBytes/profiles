package com.chris.profiles.data.posts.repositories

import arrow.Kind
import arrow.effects.typeclasses.MonadDefer
import com.chris.profiles.data.posts.repositories.mappers.PostMapper
import com.chris.profiles.datasources.network.posts.netsources.PostsNetServiceApi
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.posts.models.Post

class PostsRepository<F>(
    private val postsNetServiceApi: PostsNetServiceApi<F>,
    private val monadDefer: MonadDefer<F>,
    private val postMapper: PostMapper = PostMapper
) : PostsRepositoryApi<F>, MonadDefer<F> by monadDefer {

    override fun getPost(postId: Int): Kind<F, Post> {
        return postsNetServiceApi.getPost(postId)
            .map { postMapper.toDomainPost(it) }
    }

    override fun getPosts(): Kind<F, List<Post>> {
        return postsNetServiceApi.getPosts()
            .map { posts ->
                posts.map { postMapper.toDomainPost(it) }
            }
    }
}