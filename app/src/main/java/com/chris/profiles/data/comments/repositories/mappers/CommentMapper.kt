package com.chris.profiles.data.comments.repositories.mappers

import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet
import com.chris.profiles.domain.comments.models.Comment

object CommentMapper {
    fun toComment(commentNet: CommentNet): Comment {
        return with(commentNet) {
            Comment(
                body ?: "",
                email ?: "",
                id ?: 0,
                name ?: "",
                postId ?: 0
            )
        }
    }
}