package com.chris.profiles.data.users.repositories

import arrow.Kind
import arrow.effects.typeclasses.MonadDefer
import com.chris.profiles.data.users.repositories.mappers.UserMapper
import com.chris.profiles.datasources.network.users.netsources.UsersNetServiceApi
import com.chris.profiles.domain.users.UsersRepositoryApi
import com.chris.profiles.domain.users.models.User

class UsersRepository<F>(
    private val usersNetServiceApi: UsersNetServiceApi<F>,
    private val monadDefer: MonadDefer<F>,
    private val userMapper: UserMapper = UserMapper
) : UsersRepositoryApi<F>, MonadDefer<F> by monadDefer {

    override fun getUser(userId: Int): Kind<F, User> {
        return usersNetServiceApi.getUser(userId)
            .map { userMapper.toDomainUser(it) }
    }
}