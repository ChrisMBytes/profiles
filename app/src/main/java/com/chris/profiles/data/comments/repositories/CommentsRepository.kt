package com.chris.profiles.data.comments.repositories

import arrow.Kind
import arrow.effects.typeclasses.MonadDefer
import com.chris.profiles.data.comments.repositories.mappers.CommentMapper
import com.chris.profiles.datasources.network.comments.netsources.CommentsNetServiceApi
import com.chris.profiles.domain.comments.CommentsRepositoryApi
import com.chris.profiles.domain.comments.models.Comment

class CommentsRepository<F>(
    private val commentsNetServiceApi: CommentsNetServiceApi<F>,
    private val monadDefer: MonadDefer<F>,
    private val commentMapper: CommentMapper = CommentMapper
) : CommentsRepositoryApi<F>, MonadDefer<F> by monadDefer {

    override fun getComments(email: String): Kind<F, List<Comment>> {
        return commentsNetServiceApi.getComments(email)
            .map { comments ->
                comments.map { commentMapper.toComment(it) }
            }
    }
}