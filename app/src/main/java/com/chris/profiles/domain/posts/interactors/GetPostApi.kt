package com.chris.profiles.domain.posts.interactors

import arrow.Kind
import com.chris.profiles.domain.posts.models.Post

interface GetPostApi<F> {
    fun getPost(postId: Int): Kind<F, Post>
}