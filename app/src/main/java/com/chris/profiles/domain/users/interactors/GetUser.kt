package com.chris.profiles.domain.users.interactors

import arrow.Kind
import com.chris.profiles.domain.users.UsersRepositoryApi
import com.chris.profiles.domain.users.models.User

class GetUser<F>(
    private val usersRepositoryApi: UsersRepositoryApi<F>
) : GetUserApi<F> {
    override fun getUser(userId: Int): Kind<F, User> {
        return usersRepositoryApi.getUser(userId)
    }
}