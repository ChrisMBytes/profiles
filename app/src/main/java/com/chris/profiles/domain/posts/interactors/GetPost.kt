package com.chris.profiles.domain.posts.interactors

import arrow.Kind
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.posts.models.Post

class GetPost<F>(
    private val repositoryApi: PostsRepositoryApi<F>
) : GetPostApi<F> {
    override fun getPost(postId: Int): Kind<F, Post> {
        return repositoryApi.getPost(postId)
    }
}