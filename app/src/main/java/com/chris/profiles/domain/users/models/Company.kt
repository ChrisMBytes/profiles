package com.chris.profiles.domain.users.models

data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)