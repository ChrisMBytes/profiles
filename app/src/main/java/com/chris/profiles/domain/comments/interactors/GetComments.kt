package com.chris.profiles.domain.comments.interactors

import arrow.Kind
import com.chris.profiles.domain.comments.CommentsRepositoryApi
import com.chris.profiles.domain.comments.models.Comment

class GetComments<F>(
    private val commentsRepositoryApi: CommentsRepositoryApi<F>
) : GetCommentsApi<F> {
    override fun getComments(email: String): Kind<F, List<Comment>> {
        return commentsRepositoryApi.getComments(email)
    }
}