package com.chris.profiles.domain.users

import arrow.Kind
import com.chris.profiles.domain.users.models.User

interface UsersRepositoryApi<F> {
    fun getUser(userId: Int): Kind<F, User>
}