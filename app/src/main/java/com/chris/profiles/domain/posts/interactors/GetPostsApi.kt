package com.chris.profiles.domain.posts.interactors

import arrow.Kind
import com.chris.profiles.domain.posts.models.Post

interface GetPostsApi<F> {
    fun getPosts(): Kind<F, List<Post>>
}