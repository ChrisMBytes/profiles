package com.chris.profiles.domain.users.interactors

import arrow.Kind
import com.chris.profiles.domain.users.models.User

interface GetUserApi<F> {
    fun getUser(userId: Int): Kind<F, User>
}