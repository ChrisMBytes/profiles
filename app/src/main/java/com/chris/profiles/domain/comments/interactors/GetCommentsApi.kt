package com.chris.profiles.domain.comments.interactors

import arrow.Kind
import com.chris.profiles.domain.comments.models.Comment

interface GetCommentsApi<F> {
    fun getComments(email: String): Kind<F, List<Comment>>

}