package com.chris.profiles.domain.posts

import arrow.Kind
import com.chris.profiles.domain.posts.models.Post

interface PostsRepositoryApi<F> {
    fun getPosts(): Kind<F, List<Post>>
    fun getPost(postId: Int): Kind<F, Post>
}