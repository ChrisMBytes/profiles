package com.chris.profiles.domain.posts.interactors

import arrow.Kind
import com.chris.profiles.domain.posts.PostsRepositoryApi
import com.chris.profiles.domain.posts.models.Post

class GetPosts<F>(
    private val repositoryApi: PostsRepositoryApi<F>
) : GetPostsApi<F> {

    override fun getPosts(): Kind<F, List<Post>> {
        return repositoryApi.getPosts()
    }
}