package com.chris.profiles.domain.users.models

data class Geo(
    val lat: String,
    val lng: String
)