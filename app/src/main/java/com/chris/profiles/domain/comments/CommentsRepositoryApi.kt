package com.chris.profiles.domain.comments

import arrow.Kind
import com.chris.profiles.domain.comments.models.Comment

interface CommentsRepositoryApi<F> {
    fun getComments(email: String): Kind<F, List<Comment>>
}