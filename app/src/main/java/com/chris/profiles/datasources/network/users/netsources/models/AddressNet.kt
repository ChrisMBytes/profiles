package com.chris.profiles.datasources.network.users.netsources.models

data class AddressNet(
    val city: String? = null,
    val geo: GeoNet? = null,
    val street: String? = null,
    val suite: String? = null,
    val zipcode: String? = null
)