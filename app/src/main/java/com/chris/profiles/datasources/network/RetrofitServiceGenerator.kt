package com.chris.profiles.datasources.network

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitServiceGenerator(private val retrofitConfig: RetrofitConfig) : ServiceGenerator {
    private lateinit var retrofit: Retrofit

    init {
        initialise()
    }

    private fun initialise() {
        retrofit = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(retrofitConfig.baseUrl)
            .build()
    }

    override fun <Service> createService(serviceClass: Class<Service>): Service {
        return retrofit.create(serviceClass)
    }
}
