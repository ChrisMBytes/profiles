package com.chris.profiles.datasources.network.posts.netsources

import arrow.Kind
import arrow.core.Either
import arrow.effects.typeclasses.MonadDefer
import com.chris.profiles.datasources.network.posts.PostsServiceApi
import com.chris.profiles.datasources.network.posts.netsources.models.PostNet

class PostsNetService<F>(
    private val postsServiceApi: PostsServiceApi,
    private val monadDefer: MonadDefer<F>
) : PostsNetServiceApi<F>, MonadDefer<F> by monadDefer {

    override fun getPost(postId: Int): Kind<F, PostNet> {
        return deferUnsafe {
            postsServiceApi.getPost(postId).execute().run {
                when {
                    isSuccessful -> Either.right(this.body()?.get(0) ?: PostNet())
                    else -> Either.left(Exception(this.message()))
                }
            }
        }
    }

    override fun getPosts(): Kind<F, List<PostNet>> {
        return deferUnsafe {
            postsServiceApi.getPosts().execute().run {
                when {
                    isSuccessful -> Either.right(this.body() ?: emptyList())
                    else -> Either.left(Exception(this.message()))
                }
            }
        }
    }
}