package com.chris.profiles.datasources.network.comments.netsources

import arrow.Kind
import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet

interface CommentsNetServiceApi<F> {
    fun getComments(email: String): Kind<F, List<CommentNet>>
}