package com.chris.profiles.datasources.network.users

import com.chris.profiles.datasources.network.users.netsources.models.UserNet
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersServiceApi {
    @GET("users")
    fun getUser(@Query("id") userId: Int): Call<List<UserNet>>
}