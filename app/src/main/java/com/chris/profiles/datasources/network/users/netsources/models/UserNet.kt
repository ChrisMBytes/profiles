package com.chris.profiles.datasources.network.users.netsources.models

data class UserNet(
    val address: AddressNet? = null,
    val company: CompanyNet? = null,
    val email: String? = null,
    val id: Int? = null,
    val name: String? = null,
    val phone: String? = null,
    val username: String? = null,
    val website: String? = null
)