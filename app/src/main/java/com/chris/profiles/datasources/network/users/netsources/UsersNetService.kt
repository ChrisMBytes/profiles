package com.chris.profiles.datasources.network.users.netsources

import arrow.Kind
import arrow.core.Either
import arrow.effects.typeclasses.MonadDefer
import com.chris.profiles.datasources.network.users.UsersServiceApi
import com.chris.profiles.datasources.network.users.netsources.models.UserNet

class UsersNetService<F>(
    private val usersServiceApi: UsersServiceApi,
    private val monadDefer: MonadDefer<F>
) : UsersNetServiceApi<F>, MonadDefer<F> by monadDefer {

    override fun getUser(userId: Int): Kind<F, UserNet> {
        return deferUnsafe {
            usersServiceApi.getUser(userId).execute().run {
                when {
                    isSuccessful -> Either.right(this.body()?.get(0) ?: UserNet())
                    else -> Either.left(Exception(this.message()))
                }
            }
        }
    }
}