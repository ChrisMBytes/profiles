package com.chris.profiles.datasources.network.users.netsources.models

data class CompanyNet(
    val bs: String? = null,
    val catchPhrase: String? = null,
    val name: String? = null
)