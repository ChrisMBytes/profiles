package com.chris.profiles.datasources.network.users.netsources

import arrow.Kind
import com.chris.profiles.datasources.network.users.netsources.models.UserNet

interface UsersNetServiceApi<F> {
    fun getUser(userId: Int): Kind<F, UserNet>
}