package com.chris.profiles.datasources.network

import com.chris.profiles.BuildConfig.SERVER_URL

class ProfilesConfig : RetrofitConfig {

    override val baseUrl: String
        get() = SERVER_URL
}