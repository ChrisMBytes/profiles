package com.chris.profiles.datasources.network.comments.netsources.models

data class CommentNet(
    val body: String?,
    val email: String?,
    val id: Int?,
    val name: String?,
    val postId: Int?
)