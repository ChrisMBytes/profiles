package com.chris.profiles.datasources.network.comments.netsources

import arrow.Kind
import arrow.core.Either
import arrow.effects.typeclasses.MonadDefer
import com.chris.profiles.datasources.network.comments.CommentsServiceApi
import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet

class CommentsNetService<F>(
    private val commentsServiceApi: CommentsServiceApi,
    private val monadDefer: MonadDefer<F>
) : CommentsNetServiceApi<F>, MonadDefer<F> by monadDefer {

    override fun getComments(email: String): Kind<F, List<CommentNet>> {
        return deferUnsafe {
            commentsServiceApi.getComments(email).execute().run {
                when {
                    isSuccessful -> Either.right(this.body() ?: emptyList())
                    else -> Either.left(Exception(this.message()))
                }
            }
        }
    }
}