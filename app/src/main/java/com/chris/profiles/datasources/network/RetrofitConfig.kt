package com.chris.profiles.datasources.network

interface RetrofitConfig {
    val baseUrl: String
}
