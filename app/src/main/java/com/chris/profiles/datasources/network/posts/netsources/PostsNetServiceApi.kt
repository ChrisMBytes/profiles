package com.chris.profiles.datasources.network.posts.netsources

import arrow.Kind
import com.chris.profiles.datasources.network.posts.netsources.models.PostNet

interface PostsNetServiceApi<F> {
    fun getPosts(): Kind<F, List<PostNet>>
    fun getPost(postId: Int): Kind<F, PostNet>
}