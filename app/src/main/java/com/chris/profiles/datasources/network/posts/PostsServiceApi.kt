package com.chris.profiles.datasources.network.posts

import com.chris.profiles.datasources.network.posts.netsources.models.PostNet
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PostsServiceApi {
    @GET("posts")
    fun getPosts(): Call<List<PostNet>>

    @GET("posts")
    fun getPost(@Query("id") id: Int): Call<List<PostNet>>
}