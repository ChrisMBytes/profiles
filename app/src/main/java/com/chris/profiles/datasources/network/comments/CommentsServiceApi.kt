package com.chris.profiles.datasources.network.comments

import com.chris.profiles.datasources.network.comments.netsources.models.CommentNet
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CommentsServiceApi {
    @GET("comments")
    fun getComments(@Query("email") email: String): Call<List<CommentNet>>
}