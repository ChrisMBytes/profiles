package com.chris.profiles.datasources.network.posts.netsources.models

import com.squareup.moshi.Json

data class PostNet(
    @Json(name = "userId")
    val userId: Int? = null,
    @Json(name = "id")
    val id: Int? = null,
    @Json(name = "title")
    val title: String? = null,
    @Json(name = "body")
    val body: String? = null
)