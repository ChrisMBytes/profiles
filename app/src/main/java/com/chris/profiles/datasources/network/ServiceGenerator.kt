package com.chris.profiles.datasources.network

interface ServiceGenerator {
    fun <Service> createService(serviceClass: Class<Service>): Service
}
