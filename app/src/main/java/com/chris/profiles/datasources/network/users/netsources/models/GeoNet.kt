package com.chris.profiles.datasources.network.users.netsources.models

data class GeoNet(
    val lat: String? = null,
    val lng: String? = null
)