package com.chris.profiles.view.recyclerview.navigator

import android.content.Context
import android.content.Intent
import com.chris.profiles.view.postprofile.PostProfileActivity
import com.chris.profiles.view.postprofile.PostProfileActivity.Intent.EXTRA_POST_ID
import com.chris.profiles.view.postprofile.PostProfileActivity.Intent.EXTRA_USER_ID

class ItemNavigator {

    companion object {
        @JvmStatic
        fun goToPostProfile(context: Context, userId: Int?, postId: Int?) {
            Intent(context, PostProfileActivity::class.java).apply {
                putExtra(EXTRA_POST_ID, postId)
                putExtra(EXTRA_USER_ID, userId)
                context.startActivity(this)
            }
        }
    }

}