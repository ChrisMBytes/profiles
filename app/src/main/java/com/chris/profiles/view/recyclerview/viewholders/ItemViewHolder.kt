package com.chris.profiles.view.recyclerview.viewholders

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.chris.profiles.presentation.recyclerview.viewmodels.ItemViewModel

open class ItemViewHolder<T, VT : ItemViewModel<T>>(
    itemView: View,
    private val binding: ViewDataBinding,
    private val viewModel: VT
) : RecyclerView.ViewHolder(itemView) {

    fun setItem(item: T) {
        viewModel.setItem(item)
        binding.executePendingBindings()
    }
}