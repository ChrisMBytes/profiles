package com.chris.profiles.view.recyclerview.adapters

import androidx.recyclerview.widget.RecyclerView
import com.chris.profiles.presentation.recyclerview.viewmodels.ItemViewModel
import com.chris.profiles.view.recyclerview.viewholders.ItemViewHolder

abstract class DataBindingRecyclerViewAdapter<ITEM, VIEW_MODEL : ItemViewModel<ITEM>>(
    private val items: List<ITEM>
) : RecyclerView.Adapter<ItemViewHolder<ITEM, VIEW_MODEL>>() {

    override fun onBindViewHolder(holder: ItemViewHolder<ITEM, VIEW_MODEL>, position: Int) {
        holder.setItem(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
