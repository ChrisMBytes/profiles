package com.chris.profiles.view.recyclerview.posts.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.chris.profiles.R
import com.chris.profiles.databinding.PostItemBinding
import com.chris.profiles.domain.posts.models.Post
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModel
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModelApi
import com.chris.profiles.view.recyclerview.adapters.DataBindingRecyclerViewAdapter
import com.chris.profiles.view.recyclerview.posts.viewholders.PostViewHolder
import com.chris.profiles.view.recyclerview.viewholders.ItemViewHolder

class PostItemAdapter(
    posts: List<Post>
) : DataBindingRecyclerViewAdapter<Post, PostItemViewModelApi>(posts) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewHolder<Post, PostItemViewModelApi> {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.post_item, parent, false)
        val viewModel = PostItemViewModel()
        val binding = PostItemBinding.bind(itemView)
        binding.viewModel = viewModel

        return PostViewHolder(itemView, binding, viewModel)
    }
}