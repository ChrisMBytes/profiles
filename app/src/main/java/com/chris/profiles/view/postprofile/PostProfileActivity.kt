package com.chris.profiles.view.postprofile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.chris.profiles.R
import com.chris.profiles.databinding.ActivityPostProfileBinding
import com.chris.profiles.presentation.postprofile.viewmodels.PostProfileViewModelApi
import dagger.android.AndroidInjection
import javax.inject.Inject


class PostProfileActivity : AppCompatActivity() {
    companion object Intent {
        const val EXTRA_POST_ID = "post_id"
        const val EXTRA_USER_ID = "user_id"
    }

    @Inject
    lateinit var postProfileViewModelApi: PostProfileViewModelApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityPostProfileBinding>(
            this,
            R.layout.activity_post_profile
        )
        binding.lifecycleOwner = this
        AndroidInjection.inject(this)
        binding.viewModel = postProfileViewModelApi
        setup()
    }

    private fun setup() {
        val extras = intent.extras ?: return
        with(extras) {
            postProfileViewModelApi.apply {
                initUser(getInt(EXTRA_USER_ID))
                initUserPost(getInt(EXTRA_POST_ID))
            }
        }
        postProfileViewModelApi.email.observe({ this@PostProfileActivity.lifecycle }) {
            postProfileViewModelApi.initComments(it)
        }
    }

    override fun onDestroy() {
        postProfileViewModelApi.clear()
        super.onDestroy()
    }
}