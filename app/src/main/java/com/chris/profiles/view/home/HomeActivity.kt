package com.chris.profiles.view.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.chris.profiles.R
import com.chris.profiles.databinding.ActivityMainBinding
import com.chris.profiles.presentation.home.viewmodels.HomeViewModelApi
import com.chris.profiles.view.recyclerview.posts.adapters.PostItemAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject

class HomeActivity : AppCompatActivity() {

    @Inject
    lateinit var homeViewModelApi: HomeViewModelApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )
        binding.lifecycleOwner = this
        AndroidInjection.inject(this)
        setup(binding)
    }

    private fun setup(binding: ActivityMainBinding) {
        homeViewModelApi.posts.observe({ this.lifecycle }) {
            if (binding.postsRecyclerView.adapter == null) {
                binding.postsRecyclerView.layoutManager = LinearLayoutManager(this)
                binding.postsRecyclerView.adapter = PostItemAdapter(it)
            }
        }
    }

    override fun onDestroy() {
        homeViewModelApi.clear()
        super.onDestroy()
    }
}
