package com.chris.profiles.view.recyclerview.posts.viewholders

import android.view.View
import androidx.databinding.ViewDataBinding
import com.chris.profiles.domain.posts.models.Post
import com.chris.profiles.presentation.recyclerview.posts.viewmodels.PostItemViewModelApi
import com.chris.profiles.view.recyclerview.viewholders.ItemViewHolder

class PostViewHolder(
    itemView: View,
    binding: ViewDataBinding,
    viewModel: PostItemViewModelApi
) : ItemViewHolder<Post, PostItemViewModelApi>(itemView, binding, viewModel)